package pstoretrail

import (
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"time"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"fmt"
	"encoding/json"
)

type DynamoDbSink struct {
	Client *dynamodb.DynamoDB
	TableName string
}

func (ds *DynamoDbSink) Consume(records <-chan dynamoRecord) error {
	batched := make(chan []dynamoRecord)
	go batchIt(records, batched, 25, time.Second)

	for batch := range batched {
		writeReqs := []*dynamodb.WriteRequest{}
		for _, record := range batch {
			record := record
			av, err := dynamodbattribute.MarshalMap(record)
			if err != nil {
				panic(fmt.Sprintf("failed to DynamoDB marshal Record, %v", err))
			}
			writeReqs = append(writeReqs, &dynamodb.WriteRequest{
				PutRequest: &dynamodb.PutRequest{Item: av},
			})
		}

		reqItems := map[string][]*dynamodb.WriteRequest{}
		reqItems[ds.TableName] = writeReqs

		jsonLog("dynamo req", reqItems)
		resp, err := ds.Client.BatchWriteItem(&dynamodb.BatchWriteItemInput{RequestItems: reqItems})
		jsonLog("dynamo resp", resp)
		if err != nil {
			jsonLog("dynamo err", fmt.Sprintf(`"%s"`, err.Error()))
			return err
		}
		// TODO: do something with unprocessed items
	}
	return nil
}

func jsonLog(msg string, obj interface{}) {
	bytes, _ := json.Marshal(obj)
	fmt.Printf(`{"msg": "%s", "obj": %s}`, msg, string(bytes))
	fmt.Println("")
}

func batchIt(input <-chan dynamoRecord, output chan<- []dynamoRecord, size int, threshold time.Duration) {
	basket := []dynamoRecord{}

	flush := func() {
		if len(basket) > 0 {
			output <- basket
			basket = []dynamoRecord{}
		}
	}

	for {
		select {
		case <- time.After(threshold):
			flush()
		case record, ok := <- input:
			if !ok {
				flush()
				close(output)
				return
			}
			basket = append(basket, record)
			if len(basket) == size {
				flush()
			}
		}
	}
}