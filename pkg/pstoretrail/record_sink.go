package pstoretrail

type RecordSink interface {
	Consume(<-chan dynamoRecord) error
}
