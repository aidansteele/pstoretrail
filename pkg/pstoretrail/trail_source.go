package pstoretrail

import (
	"os"
	"bufio"
	"encoding/json"
)

type Trail interface{}

type TrailSource interface {
	Produce(chan<- Trail) error
}

type FileSource struct {
	File *os.File
}

func (fs *FileSource) Produce(output chan<- Trail) error {
	scanner := bufio.NewScanner(fs.File)

	for scanner.Scan() {
		line := scanner.Bytes()
		var target Trail
		json.Unmarshal(line, &target)
		if target != nil {
			output <- target
		}
	}

	close(output)
	return nil
}

type SliceSource struct {
	Slice []Trail
}

func (ss *SliceSource) Produce(output chan<- Trail) error {
	for _, trail := range ss.Slice {
		output <- trail
	}

	close(output)
	return nil
}
