package pstoretrail

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/aws/session"
	"os"
	"bufio"
)

type dynamoRecord struct {
	ParameterArn string
	Timestamp    string
	Trail        interface{}
}

func recordsFromTrail(trail interface{}) []dynamoRecord {
	records := []dynamoRecord{}

	trailMap := trail.(map[string]interface{})
	eventSource := trailMap["eventSource"].(string)

	if eventSource != "ssm.amazonaws.com" {
		return records
	}

	relevantEvents := []string{
		"GetParameters",
		"GetParameter",
		"PutParameter",
		"GetParametersByPath",
		"DeleteParameter",
		"DeleteParameters",
	}

	eventName := trailMap["eventName"].(string)
	if !stringInSlice(eventName, relevantEvents) {
		return records
	}

	timestamp := trailMap["eventTime"].(string)

	resources := trailMap["resources"].([]interface{})
	for _, resource := range resources {
		resMap := resource.(map[string]interface{})
		arn := resMap["ARN"].(string)
		records = append(records, dynamoRecord{ParameterArn: arn, Timestamp: timestamp, Trail: trail})
	}

	return records
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func getTrails(output chan interface{}) {
	file, _ := os.Open("out")
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Bytes()
		var target interface{}
		json.Unmarshal(line, &target)
		if target != nil {
			output <- target
		}
	}

	close(output)
}

func SourceToSink(source TrailSource, sink RecordSink) {
	trails := make(chan Trail)
	go source.Produce(trails)

	records := make(chan dynamoRecord)

	go func() {
		for trail := range trails {
			trail_records := recordsFromTrail(trail)
			for _, record := range trail_records {
				records <- record
			}
		}
		close(records)
	}()

	sink.Consume(records)
}

func Read() {
	records := []dynamoRecord{}

	svc := dynamodb.New(session.Must(session.NewSession()))

	err := svc.ScanPages(&dynamodb.ScanInput{
		TableName: aws.String("aidan-test-pstore"),
	}, func(page *dynamodb.ScanOutput, last bool) bool {
		recs := []dynamoRecord{}

		err := dynamodbattribute.UnmarshalListOfMaps(page.Items, &recs)
		if err != nil {
			panic(fmt.Sprintf("failed to unmarshal Dynamodb Scan Items, %v", err))
		}

		records = append(records, recs...)

		return true // keep paging
	})

	if err != nil {
		panic(err)
	}
	//spew.Dump(records)
}

/*
{
  "eventVersion": "1.04",
  "userIdentity": {
    "type": "AssumedRole",
    "principalId": "AROAJFGLD7ZYSFSDIBBDU:i-039bfafde4fe24ca5",
    "arn": "arn:aws:sts::966283773129:assumed-role/XeroAppServerPrintService/i-039bfafde4fe24ca5",
    "accountId": "966283773129",
    "accessKeyId": "ASIAIOFOVUI2NB4OTWKQ",
    "sessionContext": {
      "attributes": {
        "mfaAuthenticated": "false",
        "creationDate": "2018-04-10T13:24:15Z"
      },
      "sessionIssuer": {
        "type": "Role",
        "principalId": "AROAJFGLD7ZYSFSDIBBDU",
        "arn": "arn:aws:iam::966283773129:role/XeroAppServerPrintService",
        "accountId": "966283773129",
        "userName": "XeroAppServerPrintService"
      }
    }
  },
  "eventTime": "2018-04-10T13:59:59Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "GetParameters",
  "awsRegion": "us-east-1",
  "sourceIPAddress": "54.152.8.238",
  "userAgent": "aws-sdk-dotnet-45/3.3.3.1 aws-sdk-dotnet-core/3.3.17.0 .NET_Runtime/4.0 .NET_Framework/4.0 OS/Microsoft_Windows_NT_6.2.9200.0 ClientSync",
  "requestParameters": {
    "withDecryption": true,
    "names": [
      "/dataeng/xeropdfs/secret"
    ]
  },
  "responseElements": null,
  "requestID": "74f67ef0-3cc7-11e8-8799-3596e73e91d8",
  "eventID": "0518a788-fe30-447b-b571-51d95d63a4bc",
  "readOnly": true,
  "resources": [
    {
      "ARN": "arn:aws:ssm:us-east-1:966283773129:parameter/dataeng/xeropdfs/secret",
      "accountId": "966283773129"
    }
  ],
  "eventType": "AwsApiCall",
  "recipientAccountId": "966283773129"
}
{
  "eventVersion": "1.04",
  "userIdentity": {...},
  "eventTime": "2018-04-10T13:59:58Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "GetParameter",
  "awsRegion": "us-west-2",
  "sourceIPAddress": "35.163.209.161",
  "userAgent": "Boto3/1.7.0 Python/3.6.1 Linux/4.9.85-38.58.amzn1.x86_64 exec-env/AWS_Lambda_python3.6 Botocore/1.10.0",
  "requestParameters": {
    "name": "/cloud-data/dynamite/xero-prac-uat/account-number"
  },
  "responseElements": null,
  "requestID": "7493c5c9-3cc7-11e8-a630-cd2320638706",
  "eventID": "ffc93d6c-9200-4503-8687-4540f484479e",
  "readOnly": true,
  "resources": [
    {
      "ARN": "arn:aws:ssm:us-west-2:640077214053:parameter/cloud-data/dynamite/xero-prac-uat/account-number",
      "accountId": "640077214053"
    }
  ],
  "eventType": "AwsApiCall",
  "recipientAccountId": "640077214053"
}
{
  "eventVersion": "1.04",
  "userIdentity": {...},
  "eventTime": "2018-04-10T13:59:59Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "PutParameter",
  "awsRegion": "ap-southeast-2",
  "sourceIPAddress": "52.63.165.177",
  "userAgent": "aws-sdk-nodejs/2.205.0 linux/v6.10.3 exec-env/AWS_Lambda_nodejs6.10 callback",
  "requestParameters": {
    "value": "1523368799215",
    "name": "/TownClock",
    "type": "String",
    "overwrite": true
  },
  "responseElements": {
    "version": 161437
  },
  "requestID": "74fce831-3cc7-11e8-a255-8b702a798932",
  "eventID": "db322495-50c0-48ef-8c6c-684ef6e0f0b6",
  "resources": [
    {
      "ARN": "arn:aws:ssm:ap-southeast-2:046921848075:parameter/TownClock",
      "accountId": "046921848075"
    }
  ],
  "eventType": "AwsApiCall",
  "recipientAccountId": "046921848075"
}
{
  "eventVersion": "1.04",
  "userIdentity": {...},
  "eventTime": "2018-04-10T13:59:20Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "GetParametersByPath",
  "awsRegion": "us-west-2",
  "sourceIPAddress": "35.160.193.154",
  "userAgent": "aws-sdk-nodejs/2.223.1 linux/v6.10.3 exec-env/AWS_Lambda_nodejs6.10 callback",
  "requestParameters": {
    "withDecryption": true,
    "recursive": true,
    "path": "/XeroCardAPI/",
    "nextToken": "AAEAAZ8vjYikydPyQebAg79qH8H67YyALvuIkWsBeYVdoUgoAAAAAFrMwzjj1ow6D+rVM1CbDmH3ic1DEKzeBSDfw8PM7RkeUeba6Migf7uRtcTimc994WZZo2JPG9uMyZtgVA+WxXR3Rh4b6zcRwM/rLvRIYgXlDFenKEDteVXY9bTLX34gcEaJfDIhjT1qoz+BPaMUk1WAWU+71hlE2RBLzYDqXUT+EG2TfwkQ7D56rq3kV9J9Yg8wNrg+LYHP2dq8qY1GY6PtrvVda8WZtZezcb1CxHWWnBCBA4gdQPvkaTm6SQBZWoaxkxjaEP8QN702OSazCAINS6+3Y9gLu4dZaHWnE/uhi/2KmWDBQgfZQF5QaQN8zmyLhlJanrafeeHKP9iBSHM3DiQHsU1BM07puqNxw4jDAozVHJepiCTRgTihY1kTEb5Y68a++scZ9oF5OOQPACWw70nKabfC3DxmxrxEa/wjZYCY36JTApIXE17Sn0knk88xVYIwsTxV9RnslJeB69mo9Zyx1UPp7K623dgsrkAzgFgSdHy+cc/W5i9iQ8kqTYXep78gRXvepjNMCt+aIEDvVyj/AmcnnIv6uNzyXDw+FZ4YIejBb94dCgsiL4hG93Pp5TL6wCoh+hsC+ajOLMYMo+W5r8bvq45vAGa6ic0m8uo1P99/ggPhjVKKBqak/cObiwc="
  },
  "responseElements": null,
  "requestID": "5dace354-3cc7-11e8-bf98-c522f89cbf3c",
  "eventID": "d6f5c822-977d-4309-9519-ae0279eceb47",
  "readOnly": true,
  "resources": [
    {
      "ARN": "arn:aws:ssm:us-west-2:469928853788:parameter/XeroCardAPI/",
      "accountId": "469928853788"
    }
  ],
  "eventType": "AwsApiCall",
  "recipientAccountId": "469928853788"
}
{
  "eventVersion": "1.04",
  "userIdentity": {...},
  "eventTime": "2018-04-10T13:43:25Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "GetParameterHistory",
  "awsRegion": "us-east-1",
  "sourceIPAddress": "202.55.99.61",
  "userAgent": "console.ec2.amazonaws.com",
  "requestParameters": {
    "name": "BAS.DbUp.ConnectionString",
    "maxResults": 50,
    "withDecryption": false
  },
  "responseElements": null,
  "requestID": "2462d29c-3cc5-11e8-9dc5-3f9e7c91c8a0",
  "eventID": "c4c315a0-38b3-43ad-9cc1-8448ae592d09",
  "readOnly": true,
  "resources": [
    {
      "ARN": "arn:aws:ssm:us-east-1:174605153453:parameter/BAS.DbUp.ConnectionString",
      "accountId": "174605153453"
    }
  ],
  "eventType": "AwsApiCall",
  "recipientAccountId": "174605153453"
}
{
  "eventVersion": "1.04",
  "userIdentity": {...},
  "eventTime": "2018-04-10T13:44:51Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "DescribeParameters",
  "awsRegion": "us-east-1",
  "sourceIPAddress": "202.55.99.61",
  "userAgent": "console.ec2.amazonaws.com",
  "requestParameters": {
    "maxResults": 50,
    "parameterFilters": [
      {
        "option": "BeginsWith",
        "values": [
          "BAS"
        ],
        "key": "Name"
      }
    ]
  },
  "responseElements": null,
  "requestID": "58077e24-3cc5-11e8-9979-d70d72fe8a6a",
  "eventID": "0fb9d1bd-bc2d-4ff3-a494-9ac8326b6f7e",
  "readOnly": true,
  "resources": [],
  "eventType": "AwsApiCall",
  "recipientAccountId": "174605153453"
}
{
  "eventVersion": "1.04",
  "userIdentity": {...},
  "eventTime": "2018-04-10T04:27:32Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "DeleteParameter",
  "awsRegion": "us-east-1",
  "sourceIPAddress": "52.38.70.122",
  "userAgent": "aws-sdk-go/1.13.28 (go1.9.2; linux; amd64) APN/1.0 HashiCorp/1.0 Terraform/0.11.4",
  "requestParameters": {
    "name": "de-di-redshiftworkstationsecuritygroup"
  },
  "responseElements": null,
  "requestID": "7c66b328-3c77-11e8-9788-8b335bfc816a",
  "eventID": "6ad616d7-5537-4d0b-a453-b5e70f4d5084",
  "resources": [
    {
      "ARN": "arn:aws:ssm:us-east-1:174605153453:parameter/de-di-redshiftworkstationsecuritygroup",
      "accountId": "174605153453"
    }
  ],
  "eventType": "AwsApiCall",
  "recipientAccountId": "174605153453"
}
{
  "eventVersion": "1.04",
  "userIdentity": {...},
  "eventTime": "2018-04-10T22:02:14Z",
  "eventSource": "ssm.amazonaws.com",
  "eventName": "DeleteParameters",
  "awsRegion": "us-west-2",
  "sourceIPAddress": "175.45.87.122",
  "userAgent": "console.ec2.amazonaws.com",
  "requestParameters": {
    "names": [
      "xero-traffic-test-api-key"
    ]
  },
  "responseElements": {
    "invalidParameters": [],
    "deletedParameters": [
      "xero-traffic-test-api-key"
    ]
  },
  "requestID": "d398702a-3d0a-11e8-8c4b-31b942773117",
  "eventID": "f778afbc-ebc0-4d31-844b-f81760f6cb98",
  "resources": [
    {
      "ARN": "arn:aws:ssm:us-west-2:640077214053:parameter/xero-traffic-test-api-key",
      "accountId": "640077214053"
    }
  ],
  "eventType": "AwsApiCall",
  "recipientAccountId": "640077214053"
}

*/
