// Copyright © 2018 Aidan Steele <aidan.steele@glassechidna.com.au>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/spf13/cobra"
	"github.com/glassechidna/pstoretrail/pkg/pstoretrail"
	"os"
)

var importStdinCmd = &cobra.Command{
	Use:   "stdin",
	Short: "Import CloudTrail records on stdin",
	Long: `
Pass CloudTrail records in JSONline format (i.e. one record per line) to store
in DynamoDB.
`,
	Run: func(cmd *cobra.Command, args []string) {
		source := &pstoretrail.FileSource{File: os.Stdin}
		sink := DefaultSink()
		pstoretrail.SourceToSink(source, sink)
	},
}

func init() {
	importCmd.AddCommand(importStdinCmd)
}
