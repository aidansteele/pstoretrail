package cmd

import (
	"github.com/glassechidna/pstoretrail/pkg/pstoretrail"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/spf13/viper"
)

func DefaultSink() pstoretrail.RecordSink {
	sess := session.Must(session.NewSession())
	client := dynamodb.New(sess)
	table := viper.GetString("table")

	return &pstoretrail.DynamoDbSink{
		Client:    client,
		TableName: table,
	}
}
